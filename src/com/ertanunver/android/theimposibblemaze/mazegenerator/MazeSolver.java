package com.ertanunver.android.theimposibblemaze.mazegenerator;

import java.util.Random;
import java.util.Vector;

public class MazeSolver {

	/// yardimci elemanlar
	private Random random;
	
	public MazeSolver() {
		
		random = new Random();
		
	}
	
	/// yonler
	public static int NORTH = 1;
	public static int EAST 	= 2;
	public static int SOUTH = 3;
	public static int WEST 	= 4;
	
	/// labirent elemanlari
	private Maze maze;
	private Vector<Position> positionHistory;
	
	public void solveMaze(Maze maze) {
		
		this.maze = maze;

		/// labirent kutularini sifirliyoruz
		for (int i = 0; i < maze.width; i++) {
			for (int j = 0; j < maze.height; j++) {
				maze.box[i][j] = 0;	
			}
		}
		
		positionHistory = new Vector<Position>();
		
		Position firstPosition = new Position(maze.startPositionX, maze.startPositionY);
		positionHistory.add(firstPosition);
		maze.box[firstPosition.x][firstPosition.y] = 1;

		while (positionHistory.size() > 0) {
			
			Position lastPosition = positionHistory.lastElement();	
			if (lastPosition.x == maze.endPositionX && lastPosition.y == maze.endPositionY) {
				maze.solvedPositionHistory = positionHistory;
				break;
			}
			else {
				move();
			}
			
		}
		
		
	}
	
	
	public void move() {
		
		/// yonleri gosterecek olan vector
		Vector<Integer> direction = new Vector<Integer>();
		/// gidecegi yeni pozisyon
		Position newPosition = null;
		
		/// en son hareket edilen yeri getirir
		Position lastPosition = positionHistory.lastElement();	
		
		/// gidebilecegi yonleri hesaplar
		/// NORTH
		try {
			if (maze.box[lastPosition.x][lastPosition.y - 1] == 0  &&  maze.horizontalWall[lastPosition.x][lastPosition.y] == 0) {
				direction.add(NORTH);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		/// EAST
		try {
			if (maze.box[lastPosition.x + 1][lastPosition.y] == 0  &&  maze.verticalWall[lastPosition.x + 1][lastPosition.y] == 0) {
				direction.add(EAST);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		/// SOUTH
		try {
			if (maze.box[lastPosition.x][lastPosition.y + 1] == 0  &&  maze.horizontalWall[lastPosition.x][lastPosition.y + 1] == 0) {
				direction.add(SOUTH);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		/// WEST
		try {
			if (maze.box[lastPosition.x - 1][lastPosition.y] == 0  &&  maze.verticalWall[lastPosition.x][lastPosition.y] == 0) {
				direction.add(WEST);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		// eger gidebilecegi bit yon varsa birini secip gider
		if (direction.size() > 0) {
			
			int pathWay = direction.get(random.nextInt(direction.size()));
			
			if (pathWay == NORTH) {
				newPosition = new Position(lastPosition.x, lastPosition.y - 1);
			}
			else if (pathWay == EAST) {
				newPosition = new Position(lastPosition.x + 1, lastPosition.y);
			}
			else if (pathWay == SOUTH) {
				newPosition = new Position(lastPosition.x, lastPosition.y + 1);
			}
			else if (pathWay == WEST) {
				newPosition = new Position(lastPosition.x - 1, lastPosition.y);
			}
		
			positionHistory.add(newPosition);
			maze.box[newPosition.x][newPosition.y] = 1;
			
		}
		else {
			
			positionHistory.remove(positionHistory.lastElement());
			
		}
		
		
	}
	
}
