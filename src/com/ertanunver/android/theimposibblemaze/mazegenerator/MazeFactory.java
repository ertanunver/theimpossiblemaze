package com.ertanunver.android.theimposibblemaze.mazegenerator;

import java.util.Random;
import java.util.Vector;

public class MazeFactory {
	
	/// yardimci elemanlar
	private Random random;
	
	public MazeFactory() {
		
		random = new Random();
		
	}
	
	/// yonler
	public static int NORTH = 1;
	public static int EAST 	= 2;
	public static int SOUTH = 3;
	public static int WEST 	= 4;
	
	/// labirent elemanlari
	private Maze maze;
	private Vector<Position> positionHistory;
	
	public Maze creatMaze(int width, int height) {
		
		/// labirentimiz
		maze = new Maze(width, height);
		positionHistory = new Vector<Position>();
		
		Position firstPosition = new Position(random.nextInt(width), random.nextInt(height));
		positionHistory.add(firstPosition);
		maze.box[firstPosition.x][firstPosition.y] = 1;		
		
		while (positionHistory.size() > 0) {
					
			move();
						
		}
		
		return maze;
		
	}
	
	
	public void move() {
		
		/// yonleri gosterecek olan vector
		Vector<Integer> direction = new Vector<Integer>();
		/// gidecegi yeni pozisyon
		Position newPosition = null;
		
		/// en son hareket edilen yeri getirir
		Position lastPosition = positionHistory.lastElement();	
		
		/// gidebilecegi yonleri hesaplar
		/// NORTH
		try {
			if (maze.box[lastPosition.x][lastPosition.y - 1] == 0) {
				direction.add(NORTH);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		/// EAST
		try {
			if (maze.box[lastPosition.x + 1][lastPosition.y] == 0) {
				direction.add(EAST);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		/// SOUTH
		try {
			if (maze.box[lastPosition.x][lastPosition.y + 1] == 0) {
				direction.add(SOUTH);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		/// WEST
		try {
			if (maze.box[lastPosition.x - 1][lastPosition.y] == 0) {
				direction.add(WEST);
			}
		} catch (IndexOutOfBoundsException e) { }
		
		// eger gidebilecegi bit yon varsa birini secip gider
		if (direction.size() > 0) {
			
			int pathWay = direction.get(random.nextInt(direction.size()));
			
			if (pathWay == NORTH) {
				newPosition = new Position(lastPosition.x, lastPosition.y - 1);
				maze.horizontalWall[lastPosition.x][lastPosition.y] = 0;
			}
			else if (pathWay == EAST) {
				newPosition = new Position(lastPosition.x + 1, lastPosition.y);
				maze.verticalWall[lastPosition.x + 1][lastPosition.y] = 0;
			}
			else if (pathWay == SOUTH) {
				newPosition = new Position(lastPosition.x, lastPosition.y + 1);
				maze.horizontalWall[lastPosition.x][lastPosition.y + 1] = 0;
			}
			else if (pathWay == WEST) {
				newPosition = new Position(lastPosition.x - 1, lastPosition.y);
				maze.verticalWall[lastPosition.x][lastPosition.y] = 0;
			}
		
			positionHistory.add(newPosition);
			maze.box[newPosition.x][newPosition.y] = 1;
			
		}
		else {
			
			positionHistory.remove(positionHistory.lastElement());
			
		}
		
		
	}
	
}
