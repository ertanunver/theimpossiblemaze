package com.ertanunver.android.theimposibblemaze.mazegenerator;

import java.util.Vector;

public class Maze {
	
	public int width;
	public int height;
	
	public int[][] box;
	public int[][] horizontalWall;
	public int[][] verticalWall;
	
	public int startPositionX = -1;
	public int startPositionY = -1;
	public int endPositionX   = -1;
	public int endPositionY   = -1;
	
	public Vector<Position> solvedPositionHistory;
	
	public Maze(int width, int height) {
		
		this.width 	= width;
		this.height = height;
		
		startPositionX = 0;
		startPositionY = 0;
		endPositionX   = width - 1;
		endPositionY   = width - 1;
		
		
		box = new int[width][height];
		horizontalWall = new int[width + 1][height + 1];
		verticalWall   = new int[width + 1][height + 1];
		
		/// labirent kutularini sifirliyoruz
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				box[i][j] = 0;	
			}
		}
		
		/// labirent duvarlarini dolduruyoruz
		for (int i = 0; i < width + 1; i++) {
			for (int j = 0; j < height + 1; j++) {
				horizontalWall[i][j] = 1;
				verticalWall[i][j] = 1;
			}
		}
		
	}
	
}
