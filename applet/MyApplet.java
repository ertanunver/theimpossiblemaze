import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import com.ertanunver.java.mazegenerator.Maze;
import com.ertanunver.java.mazegenerator.MazeFactory;
import com.ertanunver.java.mazegenerator.MazeSolver;
import com.ertanunver.java.mazegenerator.Position;


public class MyApplet extends Applet implements MouseListener{

	private static final long serialVersionUID = 1L;
	
	MazeSolver mazeSolver;
	
	public static final int BOX_EDGE = 2;
	
	Maze maze;
	
	@Override
	public void init() {
		super.init();
		
		MazeFactory mazeFactory = new MazeFactory();
		mazeSolver = new MazeSolver();
		
		maze = mazeFactory.creatMaze(500, 250);
		
		addMouseListener(this);
		
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		
		g.setColor(Color.YELLOW);
		
		if (maze.solvedPositionHistory != null) {		
			for (Position position : maze.solvedPositionHistory) {
				g.fillRect(10 + (position.x * BOX_EDGE) + (1 * (position.x + 1)), 10 + (position.y * BOX_EDGE) + (1 * (position.y + 1)), BOX_EDGE, BOX_EDGE);
			}
		}
		
		g.setColor(Color.BLACK);
		
		/// yatay duvarlarini cizimi
		for (int x = 0; x < maze.width; x++) {
			for (int y = 0; y < maze.height + 1; y++) {
				if (maze.horizontalWall[x][y] == 1) {
					g.drawLine( 
									10 + (x * BOX_EDGE) + x,				// x0
									10 + (y * BOX_EDGE) + y,				// y0
									10 + (x * BOX_EDGE) + BOX_EDGE + x + 1, // x1
									10 + (y * BOX_EDGE) + y					// y1
							  );
				}
			}	
		}
		
		/// yatay duvarlarini cizimi
		for (int x = 0; x < maze.width + 1; x++) {
			for (int y = 0; y < maze.height; y++) {
				if (maze.verticalWall[x][y] == 1) {
					g.drawLine( 
									10 + (x * BOX_EDGE) + x,				// x0
									10 + (y * BOX_EDGE) + y,				// y0
									10 + (x * BOX_EDGE) + x,			 	// x1
									10 + (y * BOX_EDGE) + BOX_EDGE + y + 1  // y1
							  );
				}
			}	
		}
		
		if (maze.startPositionX != -1) {
			g.setColor(Color.GREEN);
			g.fillRect(10 + (maze.startPositionX * BOX_EDGE) + (1 * (maze.startPositionX + 1)), 10 + (maze.startPositionY * BOX_EDGE) + (1 * (maze.startPositionY + 1)), BOX_EDGE, BOX_EDGE);
		}
		
		if (maze.endPositionY != -1) {
			g.setColor(Color.RED);
			g.fillRect(10 + (maze.endPositionX * BOX_EDGE) + (1 * (maze.endPositionX + 1)), 10 + (maze.endPositionY * BOX_EDGE) + (1 * (maze.endPositionY + 1)), BOX_EDGE, BOX_EDGE);
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		if ( 10 < e.getX() && e.getX() < 10 + (maze.width * BOX_EDGE) + (maze.width * 1)     &&     10 < e.getY() && e.getY() < 10 + (maze.height * BOX_EDGE) + (maze.height * 1)) {
			
			int positionX = (e.getX() - 10) / (BOX_EDGE + 1);
			int positionY = (e.getY() - 10) / (BOX_EDGE + 1);
			
			if (e.getButton() == MouseEvent.BUTTON1) {
				maze.startPositionX = positionX;
				maze.startPositionY = positionY;
			}
			else if (e.getButton() == MouseEvent.BUTTON3) {
				maze.endPositionX = positionX;
				maze.endPositionY = positionY;
			}
			
			if (maze.startPositionX != -1 && maze.endPositionX != -1) {
				mazeSolver.solveMaze(maze);
			}
			
			repaint();
			
		}
		

		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
