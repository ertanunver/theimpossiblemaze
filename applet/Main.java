import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import com.ertanunver.java.mazegenerator.Maze;
import com.ertanunver.java.mazegenerator.MazeFactory;
import com.ertanunver.java.mazegenerator.MazeSolver;
import com.ertanunver.java.mazegenerator.Position;




public class Main {


	public static final int BOX_EDGE = 2;
	
	public static final int MAZE_WIDTH = 100;
	public static final int MAZE_HEIGHT = 100;
	
	public static void main(String[] args) throws IOException {
		
		MazeFactory mazeFactory = new MazeFactory();
		MazeSolver mazeSolver = new MazeSolver();
		Maze maze = mazeFactory.creatMaze(MAZE_WIDTH, MAZE_HEIGHT);
		mazeSolver.solveMaze(maze);
		
		BufferedImage bufferedImage = new BufferedImage(20 + (maze.width * BOX_EDGE) + (maze.width * 1), 20 + (maze.height * BOX_EDGE) + (maze.height * 1), BufferedImage.TYPE_INT_RGB);
		
		Graphics g = bufferedImage.getGraphics();
		
		g.setColor(Color.WHITE);
		
		g.fillRect(0, 0, 20 + (maze.height * BOX_EDGE) + (maze.height * 1), 20 + (maze.height * BOX_EDGE) + (maze.height * 1));
		
		g.setColor(Color.YELLOW);
		
		if (maze.solvedPositionHistory != null) {		
			for (Position position : maze.solvedPositionHistory) {
				g.fillRect(10 + (position.x * BOX_EDGE) + (1 * (position.x + 1)), 10 + (position.y * BOX_EDGE) + (1 * (position.y + 1)), BOX_EDGE, BOX_EDGE);
			}
		}
		
		g.setColor(Color.BLACK);
		
		/// yatay duvarlarini cizimi
		for (int x = 0; x < maze.width; x++) {
			for (int y = 0; y < maze.height + 1; y++) {
				if (maze.horizontalWall[x][y] == 1) {
					g.drawLine( 
									10 + (x * BOX_EDGE) + x,				// x0
									10 + (y * BOX_EDGE) + y,				// y0
									10 + (x * BOX_EDGE) + BOX_EDGE + x + 1, // x1
									10 + (y * BOX_EDGE) + y					// y1
							  );
				}
			}	
		}
		
		/// yatay duvarlarini cizimi
		for (int x = 0; x < maze.width + 1; x++) {
			for (int y = 0; y < maze.height; y++) {
				if (maze.verticalWall[x][y] == 1) {
					g.drawLine( 
									10 + (x * BOX_EDGE) + x,				// x0
									10 + (y * BOX_EDGE) + y,				// y0
									10 + (x * BOX_EDGE) + x,			 	// x1
									10 + (y * BOX_EDGE) + BOX_EDGE + y + 1  // y1
							  );
				}
			}	
		}
		
		if (maze.startPositionX != -1) {
			g.setColor(Color.GREEN);
			g.fillRect(10 + (maze.startPositionX * BOX_EDGE) + (1 * (maze.startPositionX + 1)), 10 + (maze.startPositionY * BOX_EDGE) + (1 * (maze.startPositionY + 1)), BOX_EDGE, BOX_EDGE);
		}
		
		if (maze.endPositionY != -1) {
			g.setColor(Color.RED);
			g.fillRect(10 + (maze.endPositionX * BOX_EDGE) + (1 * (maze.endPositionX + 1)), 10 + (maze.endPositionY * BOX_EDGE) + (1 * (maze.endPositionY + 1)), BOX_EDGE, BOX_EDGE);
		}
		
		Random random = new Random();
		
		ImageIO.write(bufferedImage, "png", new File("/Users/ertan/Desktop/maze-bitmap" + random.nextFloat() + ".png"));
		
		
	}

}
